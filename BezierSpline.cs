﻿using UnityEngine;
using System;

public class BezierSpline : MonoBehaviour {

    [SerializeField]
    public Vector3[] points;

    [SerializeField]
    private BezierControlPointMode[] modes;

    [SerializeField]
    private bool loop;

    public bool Loop
    {
        get { return loop; }
        set
        {
            loop = value;
            if( value == true )
            {
                modes[modes.Length - 1] = modes[0];
                SetControlPoint(0, points[0]);
            }
        }
    }

    public int ControlPointCount
    {
        get { return points.Length; }
    }

    public int CurveCount
    {
        get { return (points.Length - 1) / 3; }
    }

    public void Reset()
    {
        points = new Vector3[] {
            new Vector3(1f, 0f, 0f),
            new Vector3(2f, 0f, 0f),
            new Vector3(3f, 0f, 0f),
            new Vector3(4f, 0f, 0f)
        };

        modes = new BezierControlPointMode[] {
            BezierControlPointMode.Free,
            BezierControlPointMode.Free
        };
    }

    public void AddCurve()
    {
        // copy last point
        Vector3 point = points[points.Length - 1];

        // add three new points off the last for a new curve
        Array.Resize(ref points, points.Length + 3);
        point.x += 1f;
        points[points.Length - 3] = point;
        point.x += 1f;
        points[points.Length - 2] = point;
        point.x += 1f;
        points[points.Length - 1] = point;

        Array.Resize( ref modes, modes.Length + 1 );
        modes[modes.Length - 1] = modes[modes.Length - 2];

        // enforce contraints at the point where the new curve was added
        EnforceConstraints( points.Length - 4 );

        if( loop )
        {
            points[points.Length - 1] = points[0];
            modes[modes.Length - 1] = modes[0];
            EnforceConstraints( 0 );
        }
    }

    public Vector3 GetControlPoint( int idx )
    {
        return points[idx];
    }

    public void SetControlPoint( int idx, Vector3 point )
    {
        if( idx % 3 == 0 )
        {
            var delta = point - points[idx];
            if (loop)
            {
                if( idx == 0 )
                {
                    points[1] += delta;
                    points[points.Length - 2] += delta;
                    points[points.Length - 1] = point;
                }
                else if( idx == points.Length - 1 )
                {
                    points[0] = point;
                    points[1] += delta;
                    points[idx - 1] += delta;
                }
                else
                {
                    points[idx - 1] += delta;
                    points[idx + 1] += delta;
                }
            }
            else
            {
                if( idx > 0 )
                {
                    points[idx - 1] += delta;
                }
                if( idx + 1 > points.Length )
                {
                    points[idx + 1] += delta;
                }
            }
        }
        points[idx] = point;
        EnforceConstraints( idx );
    }

    public BezierControlPointMode GetControlPointMode( int idx )
    {
        return modes[(idx + 1) / 3];
    }

    public void SetControlPointMode( int idx, BezierControlPointMode mode )
    {
        int modeIndex = (idx + 1) / 3;
        modes[modeIndex] = mode;

        if( loop )
        {
            if( modeIndex == 0 )
            {
                modes[modes.Length - 1] = mode;
            }
            else if( modeIndex == modes.Length - 1 )
            {
                modes[0] = mode;
            }
        }

        EnforceConstraints( idx );
    }

    void EnforceConstraints(int idx)
    {
        int modeIndex = (idx + 1) / 3;
        var mode = modes[modeIndex];

        // dont enforce if mode is set to free or if we're at the start/end points of the curve
        if (mode == BezierControlPointMode.Free || !loop && (modeIndex == 0 || modeIndex == modes.Length - 1))
        {
            return;
        }

        int middleIndex = modeIndex * 3;
        int fixedIndex, enforcedIndex;
        if (idx <= middleIndex)
        {
            fixedIndex = middleIndex - 1;
            if (fixedIndex < 0)
            {
                fixedIndex = points.Length - 2;
            }
            enforcedIndex = middleIndex + 1;
            if (enforcedIndex >= points.Length)
            {
                enforcedIndex = 1;
            }
        }
        else
        {
            fixedIndex = middleIndex + 1;
            if (fixedIndex >= points.Length)
            {
                fixedIndex = 1;
            }
            enforcedIndex = middleIndex - 1;
            if (enforcedIndex < 0)
            {
                enforcedIndex = points.Length - 2;
            }
        }

        Vector3 middle = points[middleIndex];
        Vector3 enforcedTangent = middle - points[fixedIndex];
        if (mode == BezierControlPointMode.Aligned)
        {
            enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
        }
        points[enforcedIndex] = middle + enforcedTangent;

    }

    public Vector3 GetPointInWorldSpace(float t)
    {
        int i;
        if( t >= 1f )
        {
            t = 1f;
            i = points.Length - 4;
        }
        else
        {
            t = Mathf.Clamp01(t) * CurveCount;
            i = (int)t;
            t -= i;
            i *= 3;
        }

        return transform.TransformPoint( Bezier.GetPoint(
            points[i], points[i + 1], points[i + 2], points[i + 3], t ) );

        //return transform.TransformPoint(Bezier.GetPoint(points[0], points[1], points[2], points[3], t));
    }

    public Vector3 GetVelocity(float t)
    {
        int i;
		if (t >= 1f)
        {
			t = 1f;
			i = points.Length - 4;
		}
		else
        {
			t = Mathf.Clamp01(t) * CurveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}

		return transform.TransformPoint(Bezier.GetFirstDerivative(
			points[i], points[i + 1], points[i + 2], points[i + 3], t)) - transform.position;
    }

    public Vector3 GetAcceleration(float t)
    {
        int i;
        if (t >= 1f)
        {
            t = 1f;
            i = points.Length - 4;
        }
        else
        {
            t = Mathf.Clamp01(t) * CurveCount;
            i = (int)t;
            t -= i;
            i *= 3;
        }

        return transform.TransformPoint(Bezier.GetSecondDerivative(
            points[i], points[i + 1], points[i + 2], points[i + 3], t)) - transform.position;
    }

    public Vector3 GetDirection(float t)
    {
        return GetVelocity(t).normalized;
    }

    public float GetClosestPoint(Vector3 point, int iterations, float start = 0, float end = 1, float step = .01f)
    {
        return GetClosestPointIntern((splinePos) => (point - splinePos).sqrMagnitude, iterations, start, end, step);
    }

    private delegate float DistanceFunction(Vector3 splinePos);

    private float GetClosestPointIntern(DistanceFunction distFnc, int iterations, float start, float end, float step)
    {
        iterations = Mathf.Clamp(iterations, 0, 5);

        float minParam = GetClosestPointOnSegmentIntern(distFnc, start, end, step);

        for (int i = 0; i < iterations; i++)
        {
            float searchOffset = Mathf.Pow(10f, -(i + 2f));

            start = Mathf.Clamp01(minParam - searchOffset);
            end = Mathf.Clamp01(minParam + searchOffset);
            step = searchOffset * .1f;

            minParam = GetClosestPointOnSegmentIntern(distFnc, start, end, step);
        }

        return minParam;
    }

    private float GetClosestPointOnSegmentIntern(DistanceFunction distFnc, float start, float end, float step)
    {
        float minDistance = Mathf.Infinity;
        float minParam = 0f;

        for (float param = start; param <= end; param += step)
        {
            float distance = distFnc( transform.InverseTransformDirection(GetPointInWorldSpace(param)));

            if (minDistance > distance)
            {
                minDistance = distance;
                minParam = param;
            }
        }

        return minParam;
    }

}
